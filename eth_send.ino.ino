#include <Arduino.h>

#ifdef ESP8266
  #include <ESP8266WiFi.h>
#elif defined(ESP32)
  #include <WiFi.h>
#else
  #error "Unsupported board!"
#endif

#include <Web3.h>
#include <Contract.h>
#include "Conf.h"

#define USE_SERIAL Serial

string host = HOST;
string path = sPATH;
int port = 8545;
Web3 web3(&host, &path, port);

void testEth();

void setup() {

    USE_SERIAL.begin(115200);

    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }

    WiFi.begin(ENV_SSID, ENV_WIFI_KEY);

    // attempt to connect to Wifi network:
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        // wait 1 second for re-trying
        delay(1000);
    }

    USE_SERIAL.println("Connected");

    testEth();
}

void testEth() {
    string contract_address = "0xF950C84AD02EccFfD6801BA9dEa765D3a5f05b4d";
    Contract contract(&web3, &contract_address);
    strcpy(contract.options.from,"0x2eea699f0173a0381e2c797da824f7e56c668885");
    strcpy(contract.options.gasPrice,"20000000000000");
    contract.options.gas = 5000000;
    string func = "store(uint256)";
    string param = contract.SetupContractData(&func, 69);
    string result = "";
    result = contract.Send(&param);
    USE_SERIAL.println(result.c_str());
}

void loop() {
    delay(5000);
}
